FROM python:3.9.5-buster
RUN apt-get -y install git
ENV PYTHONUNBUFFERED=1
RUN git clone https://gitlab.com/mlbepub/formdocs.git
RUN pip3 install --default-timeout=300 --no-cache-dir -r ./formdocs/requirements.txt
RUN mv formdocs/data .
RUN mkdir /data/ml/django_log/
ENTRYPOINT ["python","/data/ml/classification_py_server/manage.py","runserver","0.0.0.0:8000"]