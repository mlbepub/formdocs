import torch
import torchvision
import io
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
import torchvision.transforms as transforms
from torchvision.transforms.functional import to_pil_image
from PIL import Image, ImageDraw, ImageFont
import base64
import logging


def load_pretrained_model(num_labels, device):
    mean = torch.tensor([0.8750, 0.8754, 0.8761])
    std = torch.tensor([0.1934, 0.1927, 0.1913])

    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True, progress=True,
                                                                 # min_size=800, max_size=1333,
                                                                 image_mean=mean, image_std=std,
                                                                 # RPN parameters
                                                                 rpn_anchor_generator=None, rpn_head=None,
                                                                 rpn_pre_nms_top_n_train=2000,
                                                                 rpn_pre_nms_top_n_test=1000,
                                                                 rpn_post_nms_top_n_train=2000,
                                                                 rpn_post_nms_top_n_test=1000,
                                                                 rpn_nms_thresh=0.7,
                                                                 rpn_fg_iou_thresh=0.7, rpn_bg_iou_thresh=0.3,
                                                                 rpn_batch_size_per_image=256,
                                                                 rpn_positive_fraction=0.5,
                                                                 # Box parameters
                                                                 box_roi_pool=None, box_head=None, box_predictor=None,
                                                                 box_score_thresh=0.1, box_nms_thresh=0.3,
                                                                 box_detections_per_img=100,
                                                                 box_fg_iou_thresh=0.5, box_bg_iou_thresh=0.5,
                                                                 box_batch_size_per_image=512,
                                                                 box_positive_fraction=0.05,
                                                                 bbox_reg_weights=None)

    # replace the classifier with a new one, that has
    # num_classes which is user-defined
    # num_labels = 8 definita sopra
    start_epoch = 8
    # get number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # replace the pre-trained head with a new one
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_labels)
    model_ft = model.to(device)
    # mlp_ft = mlp.to(device)
    # construct an optimizer
    params = [p for p in model_ft.parameters() if p.requires_grad]
    optimizer_ft = torch.optim.SGD(params, lr=0.001, momentum=0.9)
    # optimizer_ft = torch.optim.Adam(params)
    # and a learning rate scheduler
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer_ft, step_size=10, gamma=0.5)
    # load pretrained models and optimizer
    load_check = True
    if load_check:
        #checkpoint = torch.load('D:/ML/LAB/repository/checkpoint_deleghe/checkpoint', map_location=torch.device('cpu'))
        checkpoint = torch.load('/data/ml/models/checkpoint', map_location=torch.device('cpu'))

        #OLD##checkpoint = torch.jit.load('D:/ML/LAB/repository/checkpoint_deleghe/checkpoint')
        model_ft.load_state_dict(checkpoint['model_state_dict'])
        optimizer_ft.load_state_dict(checkpoint['optimizer_state_dict'])
    # mlp_ft.load_state_dict(checkpoint['mlp_state_dict'])
    # optimizer2.load_state_dict(checkpoint['mlp_optimizer_state_dict'])
    return model_ft


# definire parametri input
def classify(data, elabID, docID, persistOutput):
    # local debug
    #if docID == 'docB':
    #    raise Exception("DocB Test Exception")

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    LABELS_list = ['Background', 'CodiceFiscale', 'NomeCognome',
                   'Firma', 'Data', 'Firma-Box', 'Data-Box',
                   'CartaIdentitaEsterno', 'CartaIdentitaInterno',
                   'CIFronte', 'CIRetro', 'PatenteGuidaFronte',
                   'PatenteGuidaRetro', 'TesseraSanitatiaFronte',
                   'TesseraSanitariaRetro']
    LABELS = dict(zip(LABELS_list, range(0, len(LABELS_list))))
    num_labels = len(LABELS)

    model_ft = load_pretrained_model(num_labels, device)
    model_ft.eval()  # set model to evaluate mode

    ## old but gold ##
    # fileToClassify = data
    # fileToClassify = 'D:/ML/LAB/repository/test_lab/img3.jpg'   <--- originale becero
    # pil_img = Image.open(fileToClassify).convert('RGB')

    pil_img = data
    t = transforms.ToTensor()
    tt = t(pil_img)
    gpu_input = tt.to(device)
    pred = model_ft([gpu_input])

    # data returned
    data_json = {'elaborationID': elabID, 'documentID': docID}
    pred_dict = {}
    score_dict = {}
    boxes_dict = {}

    pred_class = list(pred[0]['labels'].cpu().numpy())  # Get the pred classes
    i = 0

    for pc in pred_class:
        key = f"{docID}_{str(i)}"
        pred_dict[key] = str(LABELS_list[int(pc)])
        i += 1
    data_json['predictions'] = pred_dict

    pred_scores = list(pred[0]['scores'].detach().numpy())
    i = 0
    for ps in pred_scores:
        key = f"{docID}_{str(i)}"
        score_dict[key] = str(ps)
        i += 1
    data_json['scores'] = score_dict

    pred_boxes = list(pred[0]['boxes'].detach().numpy())
    i = 0
    for pb in pred_boxes:
        key = f"{docID}_{str(i)}"
        boxes_dict[key] = str(pb)
        i += 1
    data_json['boxes'] = boxes_dict

    if persistOutput:
        annotated_img = visualize_detection(tt, pred, LABELS_list, device)
    else:
        annotated_img = None

    return data_json, annotated_img


def visualize_detection(inputs, targets, labels, device):

    _COLORS = ['#e6194b',
               '#3cb44b',
               '#ffe119',
               '#0082c8',
               '#f58231',
               '#911eb4',
               '#46f0f0',
               '#f032e6',
               '#d2f53c',
               '#fabebe',
               '#008080',
               '#000080',
               '#aa6e28',
               '#fffac8',
               '#800000',
               '#aaffc3',
               '#808000',
               '#ffd8b1',
               '#e6beff',
               '#808080',
               '#FFFFFF']

    color_map = {label: color for label, color in zip(labels, _COLORS)}

    inputs[0] = inputs[0].to(device)
    if targets is None:
        return
    targets[0]['boxes'] = targets[0]['boxes'].to(device)
    pred_class = targets[0]['labels'].to(device)
    # outputs = model_ft(inputs)

    # model_ft.eval()
    inputs[0] = inputs[0].to(device)
    # outputs = model_ft(inputs)

    # output_idx = torchvision.ops.nms(outputs[0]['boxes'],outputs[0]['scores'],0.5)
    # pred_class = outputs[0]['labels'][output_idx]
    # det_boxes = outputs[0]['boxes'][output_idx]

    det_boxes = targets[0]['boxes']
    # for ind,pred in enumerate(pred_class):
    #  if pred == 2:
    #    real_pred = eval_net_fc(net_fc,inputs,torch.tensor(det_boxes[ind]).to(device))
    #    pred_class[ind] = real_pred
    #  elif pred == 5:

    #    real_pred = eval_net_fc(net_fc,inputs,torch.tensor(det_boxes[ind]).to(device))
    #    pred_class[ind] = real_pred
    #  else:
    #    continue

    det_labels = [labels[i] for i in pred_class]

    inputs[0] = inputs[0].to('cpu')
    # trans = transforms.ToPILImage()
    # image = trans(inputs[0])
    image = to_pil_image(inputs[0]).convert("RGB")

    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype("/data/ml/classification_py_server/classification_py_server/Font/arial.ttf", size=30)
    for i in range(det_boxes.size(0)):
        # Boxes
        box_location = det_boxes[i].tolist()
        draw.rectangle(xy=box_location, outline=color_map[det_labels[i]])
        draw.rectangle(xy=[loc + 1. for loc in box_location], outline=color_map[det_labels[i]])
        # a second rectangle at an offset of 1 pixel to increase line thickness
        # draw.rectangle(xy=[l + 2. for l in box_location], outline=label_color_map[
        #     det_labels[i]])  # a third rectangle at an offset of 1 pixel to increase line thickness
        # draw.rectangle(xy=[l + 3. for l in box_location], outline=label_color_map[
        #     det_labels[i]])  # a fourth rectangle at an offset of 1 pixel to increase line thickness

        # Text
        text_size = font.getsize(det_labels[i].upper())
        text_location = [box_location[0] + 2., box_location[1] - text_size[1]]
        textbox_location = [box_location[0], box_location[1] - text_size[1], box_location[0] + text_size[0] + 4.,
                            box_location[1]]
        draw.rectangle(xy=textbox_location, fill=color_map[det_labels[i]])
        draw.text(xy=text_location, text=det_labels[i].upper(), fill='white',
                  font=font)

    del draw
    return image
