import inspect

###########################################################################################
# #####################                CLASSES               ##############################
###########################################################################################


class Immutable(object):
    def __setattr__(self, *args):
        if inspect.stack()[1][3] == '__init__':
            object.__setattr__(self, *args)
        else:
            raise TypeError('Non è possibile modificare questa istanza')
        
    __delattr__ = __setattr__


class Immagine(Immutable):
    def __init__(self, percorso, larghezza, altezza, profondita, oggetti):
        self.percorso = percorso
        self.larghezza = larghezza
        self.altezza = altezza
        self.profondita = profondita
        self.oggetti = oggetti


class Oggetto(Immutable):

    def __init__(self, label, coordinate, imgReal, imgPred):
        self.label = label
        self.coordinate = coordinate
        self.imgPred = imgPred
        self.imgReal = imgReal


class Coordinate(Immutable):
    def __init__(self, xmin, ymin, xmax, ymax):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
