from typing import AnyStr, List
from torch import nn
import torch
from PIL import Image
from torch.nn import functional as F

from .modules.transformation import TPS_SpatialTransformerNetwork
from .modules.feature_extraction import ResNet_FeatureExtractor, VGG_FeatureExtractor, RCNN_FeatureExtractor
from .modules.sequence_modeling import BidirectionalLSTM
from .modules.converter import CTCLabelConverter
from .modules.prediction import Attention
from .data import AlignCollate
from .static import config


def classify(img: Image, elabID: AnyStr, docID: AnyStr):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model_cf, ctc_converter = load_pretrained_model(device)
    # set model to evaluate mode
    model_cf.eval()
    # preprocess input image
    align_collate_fn = AlignCollate(imgH=config.imgH, imgW=config.imgW,
                                    keep_ratio_with_pad=config.PAD,
                                    maximize_contrast=config.maximize_contrast)
    _, img_b, _ = align_collate_fn([[img, img, None]])
    preds, (pred_str,) = predict_batch(model_cf, ctc_converter, img_b, device)
    # create response dict
    [confidence_score, ] = compute_confidence_score(preds)
    response = {
        "elaborationID": elabID,
        "documentID": docID,
        "predictions": {
            docID: "CodiceFiscale"
        },
        "scores": {
            docID: str(confidence_score.tolist())
        },
        "values": {
            docID: pred_str
        }
    }
    return response


def compute_confidence_score(preds):
    preds_prob = F.softmax(preds, dim=2)
    preds_max_prob, _ = preds_prob.max(dim=2)
    return [
        pred_max_prob.cumprod(dim=0)[-1]
        for pred_max_prob in preds_max_prob]


def predict_batch(model: nn.Module,
                  converter: CTCLabelConverter,
                  imgs: torch.Tensor,
                  device: torch.device):

    imgs = imgs.to(device)
    # text, length = converter.encode(labels, batch_max_length=config.batch_max_length)
    preds = model(imgs, None)
    # decode
    batch_size = imgs.size(0)
    preds_size = torch.IntTensor([preds.size(1)] * batch_size)
    _, preds_index = preds.max(2)
    preds_str = converter.decode(preds_index.data, preds_size.data)
    return preds, preds_str


def load_pretrained_model(device):

    converter = CTCLabelConverter(config.character)
    config.num_class = len(converter.character)
    model = Model(config)
    model = nn.DataParallel(model).to(device)
    if torch.cuda.is_available():
        checkpoint = torch.load(config.saved_model)
    else:
        checkpoint = torch.load(config.saved_model, map_location=torch.device('cpu'))
    model.load_state_dict(checkpoint)
    return model, converter


class Model(nn.Module):

    def __init__(self, opt):
        super(Model, self).__init__()
        self.opt = opt
        self.stages = {'Trans': opt.Transformation, 'Feat': opt.FeatureExtraction,
                       'Seq': opt.SequenceModeling, 'Pred': opt.Prediction}

        """ Transformation """
        if opt.Transformation == 'TPS':
            self.Transformation = TPS_SpatialTransformerNetwork(
                F=opt.num_fiducial,
                I_size=(opt.imgH, opt.imgW),
                I_r_size=(opt.imgH, opt.imgW),
                I_channel_num=opt.input_channel)
        else:
            print('No Transformation module specified')

        """ FeatureExtraction """
        if opt.FeatureExtraction == 'VGG':
            self.FeatureExtraction = VGG_FeatureExtractor(opt.input_channel, opt.output_channel)
        elif opt.FeatureExtraction == 'RCNN':
            self.FeatureExtraction = RCNN_FeatureExtractor(opt.input_channel, opt.output_channel)
        elif opt.FeatureExtraction == 'ResNet':
            self.FeatureExtraction = ResNet_FeatureExtractor(opt.input_channel, opt.output_channel)
        else:
            raise Exception('No FeatureExtraction module specified')
        self.FeatureExtraction_output = opt.output_channel  # int(imgH/16-1) * 512
        self.AdaptiveAvgPool = nn.AdaptiveAvgPool2d((None, 1))  # Transform final (imgH/16-1) -> 1

        """ Sequence modeling"""
        if opt.SequenceModeling == 'BiLSTM':
            self.SequenceModeling = nn.Sequential(
                BidirectionalLSTM(self.FeatureExtraction_output, opt.hidden_size, opt.hidden_size),
                BidirectionalLSTM(opt.hidden_size, opt.hidden_size, opt.hidden_size))
            self.SequenceModeling_output = opt.hidden_size
        else:
            print('No SequenceModeling module specified')
            self.SequenceModeling_output = self.FeatureExtraction_output

        """ Prediction """
        if opt.Prediction == 'CTC':
            self.Prediction = nn.Linear(self.SequenceModeling_output, opt.num_class)
        elif opt.Prediction == 'Attn':
            self.Prediction = Attention(self.SequenceModeling_output, opt.hidden_size, opt.num_class)
        else:
            raise Exception('Prediction is neither CTC or Attn')

    def forward(self, input_, text, is_train=True):
        """ Transformation stage """
        if not self.stages['Trans'] == "None":
            input_ = self.Transformation(input_)

        """ Feature extraction stage """
        visual_feature = self.FeatureExtraction(input_)
        visual_feature = self.AdaptiveAvgPool(visual_feature.permute(0, 3, 1, 2))  # [b, c, h, w] -> [b, w, c, h]
        visual_feature = visual_feature.squeeze(3)

        """ Sequence modeling stage """
        if self.stages['Seq'] == 'BiLSTM':
            contextual_feature = self.SequenceModeling(visual_feature)
        else:
            contextual_feature = visual_feature  # for convenience. this is NOT contextually modeled by BiLSTM

        """ Prediction stage """
        if self.stages['Pred'] == 'CTC':
            prediction = self.Prediction(contextual_feature.contiguous())
        else:
            prediction = self.Prediction(contextual_feature.contiguous(), text, is_train,
                                         batch_max_length=self.opt.batch_max_length)

        return prediction
