from PIL.ImageOps import autocontrast
from torch.utils.data import Dataset, DataLoader
from torch.utils.data import random_split
from torchvision import transforms
import torch
from typing import AnyStr
import pickle
import math
from PIL import Image

from .static import config


def build_dataloader():

    dataset = CustomDataset(config.data_fn, config.input_channel)
    align_collate = AlignCollate(imgH=config.imgH, imgW=config.imgW, keep_ratio_with_pad=config.PAD)
    return DataLoader(dataset, batch_size=config.batch_size, shuffle=True, num_workers=config.workers,
                      collate_fn=align_collate, pin_memory=True)


def build_test_dataloader():

    dataset = CustomDataset(config.data_fn, config.input_channel)
    test_len = math.ceil(len(dataset) * config.test_size)
    train_len = len(dataset) - test_len
    print(len(dataset), train_len, test_len)
    _, test_dataset = random_split(dataset, (train_len, test_len))
    align_collate = AlignCollate(imgH=config.imgH, imgW=config.imgW, keep_ratio_with_pad=config.PAD)
    return DataLoader(test_dataset, batch_size=config.batch_size, shuffle=True, num_workers=config.workers,
                      collate_fn=align_collate, pin_memory=True)


def batch_of_one(img: Image, label: AnyStr):

    align_collate = AlignCollate(imgH=config.imgH, imgW=config.imgW, keep_ratio_with_pad=config.PAD)
    return align_collate([[img, img, label]])


class CustomDataset(Dataset):

    def __init__(self, fp, input_channel):
        """
            classe custom per il nostro dataset
        """
        """ log = open(f'/content/drive/My Drive/{opt.exp_name}/log_dataset.txt', 'a')
        dashed_line = '-' * 80
        print(dashed_line)
        log.write(dashed_line + '\n') """
        self.input_channel = input_channel
        self.raw = dict(pickle.load(open(fp, 'rb')))
        self.imgs = list(sorted(self.raw.keys()))

    def __len__(self):
        return len(self.raw.keys())

    def __getitem__(self, index):
        key = self.imgs[index]
        img = self.raw[key]

        label = img[0].oggetti[0].label
        if self.input_channel == 1:
            real = img[0].oggetti[0].imgReal.convert('L')
            pred = img[0].oggetti[0].imgPred.convert('L')
        else:
            real = img[0].oggetti[0].imgReal
            pred = img[0].oggetti[0].imgPred
        return real, pred, label


class NormalizePAD(object):

    def __init__(self, max_size, PAD_type='right'):
        self.toTensor = transforms.ToTensor()
        self.max_size = max_size
        self.max_width_half = math.floor(max_size[2] / 2)
        self.PAD_type = PAD_type

    def __call__(self, img):
        img = self.toTensor(img)
        img.sub_(0.5).div_(0.5)
        c, h, w = img.size()
        Pad_img = torch.FloatTensor(*self.max_size).fill_(0)
        Pad_img[:, :, :w] = img  # right pad
        if self.max_size[2] != w:  # add border Pad
            Pad_img[:, :, w:] = img[:, :, w - 1].unsqueeze(2).expand(c, h, self.max_size[2] - w)

        return Pad_img


class ResizeNormalize(object):

    def __init__(self, size, interpolation=Image.BICUBIC):
        self.size = size
        self.interpolation = interpolation
        self.toTensor = transforms.ToTensor()

    def __call__(self, img):
        img = img.resize(self.size, self.interpolation)
        img = self.toTensor(img)
        img.sub_(0.5).div_(0.5)
        return img


class AlignCollate(object):

    def __init__(self, imgH=32, imgW=100, keep_ratio_with_pad=False, maximize_contrast=False):
        self.imgH = imgH
        self.imgW = imgW
        self.keep_ratio_with_pad = keep_ratio_with_pad
        self.maximize_contrast = maximize_contrast

    def __call__(self, batch):
        batch = filter(lambda x: x is not None, batch)
        reals, preds, labels = zip(*batch)
        # for reals
        images = reals
        if self.keep_ratio_with_pad:  # same concept with 'Rosetta' paper
            resized_max_w = self.imgW
            input_channel = 3 if images[0].mode == 'RGB' else 1
            transform = NormalizePAD((input_channel, self.imgH, resized_max_w))

            resized_images = []
            for image in images:
                w, h = image.size
                ratio = w / float(h)
                if math.ceil(self.imgH * ratio) > self.imgW:
                    resized_w = self.imgW
                else:
                    resized_w = math.ceil(self.imgH * ratio)

                resized_image = image.resize((resized_w, self.imgH), Image.BICUBIC)
                resized_images.append(transform(resized_image))
                # resized_image.save('./image_test/%d_test.jpg' % w)

            image_tensors = torch.cat([t.unsqueeze(0) for t in resized_images], 0)
            image_tensors_reals = image_tensors

        else:
            transform = ResizeNormalize((self.imgW, self.imgH))
            image_tensors = [transform(image) for image in images]
            image_tensors_reals = torch.cat([t.unsqueeze(0) for t in image_tensors], 0)

        # for preds
        images = preds
        if self.keep_ratio_with_pad:  # same concept with 'Rosetta' paper
            resized_max_w = self.imgW
            input_channel = 3 if images[0].mode == 'RGB' else 1
            transform = NormalizePAD((input_channel, self.imgH, resized_max_w))
            resized_images = []
            for image in images:
                w, h = image.size
                ratio = w / float(h)
                if math.ceil(self.imgH * ratio) > self.imgW:
                    resized_w = self.imgW
                else:
                    resized_w = math.ceil(self.imgH * ratio)

                resized_image = image.resize((resized_w, self.imgH), Image.BICUBIC)
                resized_images.append(transform(resized_image))
                # resized_image.save('./image_test/%d_test.jpg' % w)

            image_tensors = torch.cat([t.unsqueeze(0) for t in resized_images], 0)
            image_tensors_preds = image_tensors

        else:
            transform = ResizeNormalize((self.imgW, self.imgH))
            image_tensors = [transform(image) for image in images]
            image_tensors_preds = torch.cat([t.unsqueeze(0) for t in image_tensors], 0)

        if self.maximize_contrast:
            image_tensors_reals = autocontrast(image_tensors_reals)
            image_tensors_preds = autocontrast(image_tensors_preds)

        return image_tensors_reals, image_tensors_preds, labels
