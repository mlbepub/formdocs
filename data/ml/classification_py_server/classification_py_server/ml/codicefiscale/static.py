class Config:

    def __init__(self):
        self.exp_name = "ECF"
        self.data_fn = "/data/ml/models/datasetCF_all.pickle"
        self.manualSeed = 1234
        self.workers = 2
        self.batch_size = 16
        self.batch_max_length = 16
        self.epochs = 20
        self.valInterval = 1
        self.saved_model = "/data/ml/models/cf_checkpoint.pth"
        self.FT = True
        self.adam = True
        self.lr = 1
        self.beta1 = 0.9
        self.rho = 0.95
        self.eps = 1e-08
        self.grad_clip = 5
        self.imgH = 128
        self.imgW = 400
        self.character = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        self.PAD = True
        self.Transformation = "None"
        self.FeatureExtraction = "ResNet"
        self.SequenceModeling = "BiLSTM"
        self.Prediction = "CTC"
        self.num_fiducial = 20
        self.input_channel = 1
        self.output_channel = 512
        self.hidden_size = 256
        self.test_size = 0.2
        self.maximize_contrast = False


config = Config()
