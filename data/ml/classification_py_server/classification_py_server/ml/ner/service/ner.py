import json
from collections import defaultdict

import spacy
from pathlib import Path
import logging

logger = logging.getLogger(__name__)


def execute_ner_tag(input_text):
    # PATH in cui è salvato il modello
    model_output_dir = Path(r'/data/ml/classification_py_server/classification_py_server/ml/nertrainedmodel')

    # Caricamento modello addestrato dalla directory precedente
    logger.info("Caricamento modello dal path: ", model_output_dir)

    nlp = spacy.load(model_output_dir)

    # Test del modello addestrato
    doc = nlp(input_text)

    threshold = 0.2
    # Number of alternate analyses to consider. More is slower, and not necessarily better -- you need to experiment on your problem.
    beam_width = 16
    # This clips solutions at each step. We multiply the score of the top-ranked action by this value, and use the result as a threshold. This prevents the parser from exploring options that look very unlikely, saving a bit of efficiency. Accuracy may also improve, because we've trained on greedy objective.
    beam_density = 0.0001
    beams = nlp.entity.beam_parse([doc], beam_width, beam_density)

    entity_scores = defaultdict(float)
    result = defaultdict(list)

    for beam in beams:
        for score, ents in nlp.entity.moves.get_beam_parses(beam):
            for start, end, label in ents:
                entity_scores[(start, end, label)] += score

    for key in entity_scores:
        start, end, label = key
        score = entity_scores[key]
        if score > threshold:
            entity = str(doc[start:end])
            logger.info('Label: {}, Text: {}, Score: {}'.format(label, entity, score))
            dict = {'entity': entity, 'score': score}
            result[label].append(dict)

    return result
