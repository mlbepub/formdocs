import json
import sys
import traceback
from os import makedirs, remove, rmdir, listdir
from os.path import exists, join, dirname
from PIL import Image
from django.http import JsonResponse, HttpResponse
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.decorators import api_view, parser_classes
from .ml import classifier
from .ml.codicefiscale import classifier as cf_classifier
from datetime import datetime
import base64, io
import logging

from .ml.ner.service import ner
_TMP_DIR = "annotated_images/"
_TMP_IMG_FPP = f"{_TMP_DIR:s}/{{elab_id:s}}/{{doc_id:s}}"

logger = logging.getLogger('django.develop')

@api_view(['GET'])
def ping(request):
    """
    Ping rest controller
    """
    if request.method == 'GET':
        res = "I'm alive!"
        return Response(res, status=status.HTTP_200_OK)

    else:
        res = "Tipo di chiamata sbagliato, usa GET"
        return Response(res, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def classify(request):
    """
    Classification rest controller
    """
    elab_id = request.headers['Elaborationid']
    doc_id = request.headers['Documentid']
    persistOutput = request.headers['persistOutput']
    if persistOutput is not None:
        persistOutput = persistOutput.lower() == 'true'
    else:
        persistOutput = False;
    doc_id = request.headers['Documentid']
    dn = datetime.now()
    dn_string = dn.strftime("%d/%m/%Y %H:%M:%S")
    logger.info(f"Start Classification elab_id <{elab_id}> doc_id <{doc_id}> at {dn_string} persistOutput {persistOutput}")
    body_unicode = base64.b64decode(request.body)
    data = Image.open(io.BytesIO(body_unicode))
    error = False
    try:
        res, img = classifier.classify(data=data, elabID=elab_id, docID=doc_id, persistOutput=persistOutput)
    except:
        error = True
        img = None
        res = {'elaborationID': elab_id, 'documentID': doc_id, 'errorClass': str(sys.exc_info()[0]), 'errorMessage': str(sys.exc_info()[1])}
        logger.error(f"Error during classification elab <{elab_id}> doc_id <{doc_id}> error: {res}")
        logger.error(f"Stack Error: {traceback.print_stack()}")

    dn = datetime.now()
    dn_string = dn.strftime("%d/%m/%Y %H:%M:%S")
    logger.info(f"End Classification elab_id <{elab_id}> doc_id <{doc_id}> at {dn_string} persistOutput {persistOutput}")
    stat = status.HTTP_500_INTERNAL_SERVER_ERROR if error is True else status.HTTP_200_OK

    if img is not None:
        makedirs(join(_TMP_DIR, elab_id), exist_ok=True)
        img.save(_TMP_IMG_FPP.format(elab_id=elab_id, doc_id=doc_id), "JPEG")

    return JsonResponse(res, status=stat)


@api_view(['GET'])
def get_annotated_image(request, elab_id, doc_id):
    # make actual image-path from pattern
    fp = _TMP_IMG_FPP.format(elab_id=elab_id, doc_id=doc_id)
    # check annotated image exists
    if exists(fp):
        # base64 image encoding
        with Image.open(fp) as img:
            img_byte_arr = io.BytesIO()
            img.save(img_byte_arr, format="JPEG")
            img_b64 = base64.b64encode(img_byte_arr.getvalue())
        # remove(fp)  # delete annotated image
        return HttpResponse(status=status.HTTP_200_OK, content=img_b64)
    else:       # image not found
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['DELETE'])
def delete_annotated_image(request, elab_id, doc_id):
    fp = _TMP_IMG_FPP.format(elab_id=elab_id, doc_id=doc_id)
    remove(fp)
    dp = dirname(fp)
    if not listdir(dp):     # if directory is empty delete it as well
        rmdir(dp)
    return HttpResponse(status=status.HTTP_200_OK)


@api_view(['POST'])
def extract_fiscal_code(request):
    """
    Fiscal code extraction rest controller
    """
    dn = datetime.now()
    dn_string = dn.strftime("%d/%m/%Y %H:%M:%S")
    elab_id = request.headers['Elaborationid']
    doc_id = request.headers['Documentid']
    logger.info(f"Start Fiscal Code Extraction elab_id <{elab_id}> doc_id <{doc_id}> at {dn_string}")
    body_unicode = base64.b64decode(request.body)

    # .convert('L') --> conversione in scala di grigi per estrazione codice fiscale
    data = Image.open(io.BytesIO(body_unicode)).convert('L')
    #data = Image.open(io.BytesIO(body_unicode))

    elab_id = request.headers['Elaborationid']
    doc_id = request.headers['Documentid']
    error = False
    try:
        #utilizzo della prima rete per individuare il box contenente il codice fiscale nel documento
        res, img = classifier.classify(data=data, elabID=elab_id, docID=doc_id, persistOutput=False)
        if "predictions" in res:
            for k in res['predictions'].keys():
                if res['predictions'][k] == 'CodiceFiscale':
                    cf_coords = res["boxes"][k]
                    cf_coords = cf_coords.replace("[", "").replace("]", "").lstrip().split(" ")
                    cf_coords_conv = []
                    for v in cf_coords:
                        if v != "":
                            cf_coords_conv.append(float(v))
                    # creazioen di immagine contenente solo il codice fiscale
                    cropped_img = data.crop(cf_coords_conv)

                    # salvataggio locale
                    makedirs(join(_TMP_DIR, elab_id), exist_ok=True)
                    cropped_img.save(_TMP_IMG_FPP.format(elab_id=elab_id, doc_id=doc_id), "JPEG")

                    # estrazione del codice fiscale
                    res_cf = cf_classifier.classify(img=cropped_img, elabID=elab_id, docID=doc_id)

        else:
            res_cf = {'elaborationID': elab_id, 'documentID': doc_id, 'errorClass': 'No Fiscal Code Found',
               'errorMessage': 'Nessun codice fiscale individuato'}
                    
    except:
        error = True
        res_cf = {'elaborationID': elab_id, 'documentID': doc_id, 'errorClass': str(sys.exc_info()[0]),
               'errorMessage': str(sys.exc_info()[1])}
        logger.error(f"Error during Fiscal Code Extraction elab <{elab_id}> doc_id <{doc_id}> error: {res_cf}")

    dn = datetime.now()
    dn_string = dn.strftime("%d/%m/%Y %H:%M:%S")
    logger.info(f"End Fiscal Code Extraction elab_id <{elab_id}> doc_id <{doc_id}> at {dn_string}")
    stat = status.HTTP_500_INTERNAL_SERVER_ERROR if error is True else status.HTTP_200_OK
    return JsonResponse(res_cf, status=stat)


@api_view(['POST'])
def ner_classification(request):
    """
    NER Classification
    """
    dn = datetime.now()
    dn_string = dn.strftime("%d/%m/%Y %H:%M:%S")
    logger.info(f"Start NER classification at {dn_string}")
    elab_id = request.headers['Elaborationid']
    doc_id = request.headers['Documentid']
    error = False

    text = request.stream.body.decode('utf-8')

    try:
        tagged = ner.execute_ner_tag(text)
        res = {'elaborationID': elab_id, 'documentID': doc_id, 'entities': tagged}
        logger.debug(tagged)
    except:
        error = True
        res = {'elaborationID': elab_id, 'documentID': doc_id, 'errorClass': str(sys.exc_info()[0]), 'errorMessage': str(sys.exc_info()[1])}

    dn = datetime.now()
    dn_string = dn.strftime("%d/%m/%Y %H:%M:%S")
    logger.info(f"End NER classification elab_id <{elab_id}> doc_id <{doc_id}> at {dn_string}")
    stat = status.HTTP_500_INTERNAL_SERVER_ERROR if error is True else status.HTTP_200_OK
    return JsonResponse(res, status=stat)
