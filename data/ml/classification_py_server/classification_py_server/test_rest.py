from PIL import Image
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.decorators import api_view, parser_classes
from .ml import classifier
import base64
import io
from rest_framework.parsers import FileUploadParser


@api_view(['POST'])
def test_post_image(request):
    print("Test post image:\n")
    body_unicode = base64.b64decode(request.body)

    """salvataggio su file system"""
    # filename = "./test.jpg"
    # with open(filename, 'wb') as f:
    #    f.write(body_unicode)

    # data = Image.open(f, mode='r')
    data = Image.open(io.BytesIO(body_unicode))
    res = classifier.classify(data)

    return Response(res, status=status.HTTP_200_OK)


class ImageUploadParser(FileUploadParser):
    media_type = 'image/*'


class FileUploadView(views.APIView):
    parser_classes = (ImageUploadParser,)

    def post(self, request, format=None):
        if 'file' not in request.data:
            print("Empty content")
        file_obj = request.data['file']
        print("CIaone")
        return Response(status=204)

    def put(self, request, format=None):
        file_obj = request.data['file']
        # ...
        # do some stuff with uploaded file
        # ...
        return Response(status=204)
