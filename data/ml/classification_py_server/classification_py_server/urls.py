"""classification_py_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import rest_controller,test_rest

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('ping/', rest_controller.ping),
    path('classify', rest_controller.classify),
    path('testPost', test_rest.test_post_image),
    path('codicefiscale', rest_controller.extract_fiscal_code),
    path('annotated_image/<str:elab_id>/<str:doc_id>', rest_controller.get_annotated_image),
    path('annotated_image/delete/<str:elab_id>/<str:doc_id>', rest_controller.delete_annotated_image),
    path('ner', rest_controller.ner_classification)

]
